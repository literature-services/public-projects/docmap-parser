## sbt project compiled with Scala 3

### Usage

#### Build jar (supported by sbt-assembly plugin)
```sbt assembly```

#### Build graavlVM image
Use command:
```sbt graalvm-native-image:packageBin```

sbt-native-packager is used for building the graalvm native image(https://www.scala-sbt.org/sbt-native-packager/formats/graalvm-native-image.html), for each platform.

To build linux native image with docker build image, uncomment the line
```    graalVMNativeImageGraalVersion := Some("22.3.1"),```
in build.sbt.

### Versioning with sbt-git
https://github.com/sbt/sbt-git

### Buildinfo into version info
https://github.com/sbt/sbt-buildinfo
