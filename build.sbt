val scala3Version = "3.4.1"
val declineVersion = "2.4.1"
val circeVersion = "0.14.6"
val circeOpticsVersion = "0.15.0"

lazy val root = project
  .enablePlugins(GraalVMNativeImagePlugin)
  .enablePlugins(GitVersioning)
  .enablePlugins(BuildInfoPlugin)
  .in(file("."))
  .settings(
    name := "docmap-parser",
    version := "0.1.3-SNAPSHOT",
    scalaVersion := scala3Version,
    buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
    buildInfoPackage := "DocmapParserBuildInfo",

    libraryDependencies ++= Seq(
        "org.scala-lang.modules" %% "scala-xml" % "2.3.0",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.9.5",
        "ch.qos.logback" % "logback-classic" % "1.3.14", // 1.4.x requires Java 11
        "org.scalameta" %% "munit" % "0.7.29" % Test,
        "org.scalatest" %% "scalatest" % "3.2.15" % Test 
    ),

    libraryDependencies ++= Seq(
      "com.monovore" %% "decline" % declineVersion,
      "com.monovore" %% "decline-effect" % declineVersion,
    ),

    libraryDependencies ++= Seq(
      "io.circe" %% "circe-core",
      "io.circe" %% "circe-generic",
      "io.circe" %% "circe-parser",
    ).map(_ % circeVersion),

    libraryDependencies ++= Seq(
      "io.circe" %% "circe-optics" % circeOpticsVersion
    ),

    // graalVMNativeImageGraalVersion := Some("22.3.2"), // This take precedence over local native-image command
    graalVMNativeImageCommand := "/Library/Java/JavaVirtualMachines/graalvm-ce-java17-22.3.1/Contents/Home/bin/native-image",
    // graalVMNativeImageGraalVersion := Some("22.3.1"), // For building linux executable with docker, uncomment this line.

    // graal native image settings
    // Options used by `native-image` when building native image
    // https://www.graalvm.org/docs/reference-manual/native-image/
    graalVMNativeImageOptions ++= Seq(
      "--initialize-at-build-time", // Auto-packs dependent libs at build-time
      "--no-fallback", // Bakes-in run-time reflection (alternately: --auto-fallback, --force-fallback)
      "--no-server", // Won't be running `graalvm-native-image:packageBin` often, so one less thing to break
      // "--static", // Forces statically-linked binary, requires libc installation. Comment this out if you're using OSX
      // "--enable-url-protocols=http" // Enables http requests, which are required in order to communicate with the AWS Lambda Runtime API
      // "--enable-url-protocols=http,https" // Enables both http and https requests
    ),
    // classLoaderLayeringStrategy := ClassLoaderLayeringStrategy.Flat

    assembly / assemblyMergeStrategy := {
      case x if x.endsWith("module-info.class") =>
        MergeStrategy.discard // Safe to discard for java 8
      case x =>
        val oldStrategy = (assembly / assemblyMergeStrategy).value
        oldStrategy(x)
    },

    assembly / assemblyJarName := "docmap-parser-assembly.jar"
  )
