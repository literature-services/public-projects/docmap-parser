import java.time.LocalDate

type Url = String
type DOI = String

enum ExpressionType(val name: String) {
  case Preprint extends ExpressionType("Preprint")
  case RevisedPreprint extends ExpressionType("postprint")
  case PeerReview extends ExpressionType("review-article")
  case EvaluationSummary extends ExpressionType("evaluation-summary")
  case VersionOfRecord extends ExpressionType("version-of-record")
  case AuthorResponse extends ExpressionType("author-response")
  case UpdateSummary extends ExpressionType("update-summary")

  def valueOf(name: String): Option[ExpressionType] =
    ExpressionType.values.find(v => v.name == name)
}

enum ManifestationType(val name: String) {
  case WebPage extends ManifestationType("web-page")
}

case class Manifestation ( 
  `type`: ManifestationType,
  url: Option[Url],
  published: Option[LocalDate],
  doi: DOI,
);


case class Expression (
  `type`: ExpressionType,
  identifier: Option[String],
  versionIdentifier: Option[String],
  url: Option[Url],
  published: Option[LocalDate],
  doi: Option[DOI],
  content: Option[List[Manifestation]]

)