import scala.xml.Elem
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._
import io.circe.optics.JsonPath._
import scala.io.Source
import java.nio.file.Path
import java.nio.file.Files
import java.nio.file.Paths

// root accessor
val _docmaps = root.each.json

// docMap object accessor
val _source1 = root.provider.string
val _source2 = root.publisher.account.service.string
val _firstStep = root.`first-step`.string
val _steps = root.steps.json
val _platform = root.publisher.name.string
val _logoUrl = root.publisher.logo.string

// step object accessor
val _nextStep = root.`next-step`.string
val _inputDOI = root.inputs.each.doi.string // the "inputs" field could put under step(obselete) and action object.
val _actions = root.actions.each.json

/** Recursively get all steps following the given stepId, within the given steps
  * object
  *
  * @param steps:
  *   Json object, with each steps defined under a unique id.
  * @param stepId
  * @return
  */
def getSteps(steps: Json, stepId: String): List[Json] =
  val step = root.selectDynamic(stepId).json.getOption(steps)
  if step.isEmpty then List[Json]()
  else
    val nextStepId = _nextStep.getOption(step.get)
    if nextStepId.isEmpty then List(step.get)
    else step.get :: getSteps(steps, nextStepId.get)

/** Extract reviews from docmap json
  *
  * @param path
  * @return
  *   XML literal containing all reviews in the docmaps
  */
def docMapParser(filePath: Path, outPath: Path) =
  val jsonStr = Source.fromFile(filePath.toString()).getLines.mkString
  val parseResult = parse(jsonStr)

  parseResult match {
    case Left(failure) => {
      logger.info("Parsing JSON failed for $filePath")
      throw new Exception(s"Parsing JSON failed for $filePath") 

    }
    case Right(json) => {
      val docMaps = root.articles.json.getOption(json) // SCIETY
        .map(_docmaps.getAll(_))
        .getOrElse(root.each.docmap.json.getAll(json)) // Embo
      val xml =
        <docmap-root>
      {
          docMaps.map(docMap =>
            <docmap>
              <id>{root.id.string.getOption(docMap).getOrElse("")}</id>
          <source>{
              _source1
                .getOption(docMap)
                .getOrElse(_source2.getOption(docMap).getOrElse(""))
            }</source>
          <platform>{_platform.getOption(docMap).getOrElse("")}</platform>
          <logo-url>{_logoUrl.getOption(docMap).getOrElse("")}</logo-url>
          {
              val steps = getSteps(
                _steps.getOption(docMap).get,
                _firstStep.getOption(docMap).get
              )
              steps.map(step => {
                val actions = _actions.getAll(step)
                actions.map(action => {
                  val inputsDOI = if !root.inputs.json.getOption(action).isEmpty 
                    then _inputDOI.getAll(action) 
                    else _inputDOI.getAll(step)
                  if inputsDOI.isEmpty then
                    logger.info("Empty input DOI")
                  val participants = root.participants.each.json.getAll(action)
                  val outputs = root.outputs.each.json.getAll(action)
                  outputs.map(output => {
                    val contents = root.content.each.json.getAll(output)
                    <review>
                    <type>{
                      root.`type`.string.getOption(output).getOrElse("")
                    }</type>
                    <date>{
                      root.published.string.getOption(output).getOrElse("")
                    }</date>
                    <title>{
                      root.title.string.getOption(output).getOrElse("")
                    }</title>
                    {inputsDOI.map(doi => <doi>{doi}</doi>)}
                    {
                      contents.map(content => {
                        <url-wrapper>
                        <url>{
                          root.url.string.getOption(content).getOrElse("")
                        }</url>
                        <service>{
                          root.service.string.getOption(content).getOrElse("")
                        }</service>
                      </url-wrapper>
                      })
                    }
                    {
                      participants.map(author => {
                        <author>
                        <affiliation>{
                          root.actor.institution.string
                            .getOption(author)
                            .getOrElse("")
                        }</affiliation>
                        <role>{
                          root.role.string.getOption(author).getOrElse("")
                        }</role>
                        <family-name>{
                          root.actor.`familyName`.string
                            .getOption(author)
                            .getOrElse("")
                        }</family-name>
                        <first-name>{
                          root.actor.`firstName`.string
                            .getOption(author)
                            .getOrElse("")
                        }</first-name>
                        <name>{
                          root.actor.name.string.getOption(author).getOrElse("")
                        }</name>
                      </author>
                      })
                    }
                  </review>
                  })
                })
              })
            }
        </docmap>
          )
        }
      </docmap-root>
      if outPath != null then
        Files.createDirectories(Paths.get(s"$outPath"))
        val outFile = s"${outPath}/${filePath.getFileName()}.xml"
        logger.info(s"Output xml: $outFile")
        scala.xml.XML.save(outFile, xml)
      xml
    }
  }
