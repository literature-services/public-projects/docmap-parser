
import cats.implicits._
import com.monovore.decline._
import com.monovore.decline.effect._
import java.nio.file.Path
import java.nio.file.Paths
import java.io.File
import cats.effect._
import org.slf4j.Logger
import org.slf4j.LoggerFactory

val logger = LoggerFactory.getLogger("DocmapParser")

object DocMapParserCLI extends CommandIOApp(
  name = "docmap-parser",
  header = "EuroPMC DocMap parser"){
  override def main = {
    val fileOpt = Opts.argument[Path]("docmap_file")
    val outputOpt = Opts.option[Path]("output", short = "o", metavar = "output", help = "Set output directory")
      .withDefault(Paths.get("."))
    (fileOpt, outputOpt).mapN { (filePath, outputDir) =>
      println(s"Hello docmap. Processing $filePath ...")
      val xml = docMapParser(filePath, outputDir)
      IO(()).as(ExitCode.Success)
    }
  }
}
