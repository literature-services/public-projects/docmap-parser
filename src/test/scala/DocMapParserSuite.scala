import scala.io.Source
import scala.xml.Node
import java.nio.file.Paths
import sys.process._

class DocMapSuite extends munit.FunSuite {
  val outDir = Paths.get("./output")
  
  test("Parse embo docmap".ignore) {
  }

  test("Parse Siety docmap".ignore) {
  }

  /**
    * Domap contains one review.
    */
  test("Basic docmap parsing".only) {
    val path = Paths.get("./src/test/resources/sample-docmaps/embo.docmap")
    val xml = docMapParser(path, outDir)

    // Non empty result
    assert(xml.nonEmpty)

    // Read platform
    val platform = (xml \ "docmap" \ "platform").text
    println(platform)
    assert(platform.contains("embo press"))

    // Read reviews
    val reviews = (xml \ "docmap" \ "review")
    assert(reviews.length == 1)

    // Read provider
    val providers = (xml \ "docmap" \ "source").text
    assert(providers == "https://eeb.embo.org")
  }

  /**
    * https://www.ebi.ac.uk/panda/jira/browse/CIT-9626
    * The test docmap contains 4 peer reviews, with input doi under
    * - step
    * - action
    * - both step and action
    * - none. Should produce an error log.
    */
  test("Process new inputs field for sciety") {
    val path = Paths.get("./src/test/resources/sample-docmaps/sciety_20240425_new_inputs_location.docmap")
    val xml = docMapParser(path, outDir)
    
    // check results
    val dois = (xml \ "docmap" \ "review" \ "doi").map(_.text)
    assert(dois.length == 7)
  }
}
